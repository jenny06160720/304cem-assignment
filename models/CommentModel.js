const mongoose = require('mongoose');

const CommentStructure = mongoose.Schema({
    Userid: {
        type: String,
        required: true
    },
    Comment: {
        type: String,
        required: true,
        unique: true
    },
    CreateTime: {
        type: Date,
        default: Date.now
    },
    UpdateTime: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('comment', CommentStructure);