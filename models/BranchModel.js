const mongoose = require('mongoose');

const branchStructure = mongoose.Schema({
    enlocation: {
        type: String,
        required: true
    },
    lat: {
        type: String,
        required: true
    },
    lng: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('branch', branchStructure);