const mongoose = require('mongoose');

const UserStructure = mongoose.Schema({
    UserName: {
        type: String,
        required: true
    },
    Email: {
        type: String,
        required: true,
        unique: true
    },
    Password: {
        type: String,
        required: true
    },
    UserType: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('user', UserStructure);