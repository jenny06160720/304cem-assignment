const mongoose = require('mongoose');

const menuStructure = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    },
    urls: {
        type: String,
        required: true
    },
    locations: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('menu', menuStructure);