import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.component('Navbar', require('./components/Elements/Navbar').default);
Vue.config.productionTip = false

firebase.initializeApp({
  apiKey: "AIzaSyApJ4o7cHsznwchILxAR8lc1XlbvxgsImQ",
  authDomain: "cem-62689.firebaseapp.com",
  projectId: "cem-62689",
  storageBucket: "cem-62689.appspot.com",
  messagingSenderId: "684287125977",
  appId: "1:684287125977:web:c14d7183efc261825491c8",
  measurementId: "G-L5GNPZRZX4"
});

new Vue({
  router,
  render: h => h(App),
  created() {
  }
}).$mount('#app')
