import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Menu from '@/components/Menu'
import Branch from '@/components/Branch'
import BranchUpdate from '@/components/UpdateBranch'
import MenuUpdate from '@/components/UpdateMenu'
import User from '@/components/User'
import Profile from '@/components/Profile'
import MainPage from '@/components/MainPage'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Register',
      name: 'Register',
      component: Register
    },
    {
      path: '/menu',
      name: 'Menu',
      component: Menu
    },
    {
      path: '/branch',
      name: 'Branch',
      component: Branch
    },
    {
      path: '/updateBranch',
      name: 'updateBranch',
      component: BranchUpdate
    },
    {
      path: '/updateMenu',
      name: 'updateMenu',
      component: MenuUpdate
    },
    {
      path: '/User',
      name: 'User',
      component: User
    },
    {
      path: '/Profile',
      name: 'Profile',
      component: Profile
    },
  ]
})
