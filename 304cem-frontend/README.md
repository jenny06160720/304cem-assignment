# 304cem-frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Install
```
npm install @babel/polyfill
npm install axios
npm install bootstrap-vue
npm install mutationobserver-shim
npm install axios
npm install cors
```
