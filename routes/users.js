const express = require('express');
const router = express.Router();
const passwordHash = require('password-hash');
const UserStructure = require('../models/UserModel');

// get all user data
router.get("/", async (req, res) => {
    try {
        res.json(await UserStructure.find());
    } catch (err) {
        res.json({ message: "False" });
    }
});

// get someone user data   
router.get('/:id', async (req, res) => {
    try {
        const user = await UserStructure.findById( req.params.id);
        res.json(user);
    } catch (err) {
        res.json({ message: "False" });
    }
});

router.get('/:email/:pw', async (req, res) => {
    try {
        const user = await UserStructure.findOne({Email: req.params.email});
        if (user) {
            if (passwordHash.verify(req.params.pw, user.Password)) {
                res.json(user);
            } else {
                res.json({ message: "False" });
            }
        }
    } catch (err) {
        res.json({ message: err });
    }
});

// registeration user
router.post('/', async (req, res) => {
    const { UserName, Email, Password, UserType } = req.body;
    try {
        // check the email is exists or not
        let user = await UserStructure.findOne({ Email });
        if (user) {
            return res.status(400).json({ msg: 'User email is Exists' });
        }

        // create new user
        const pw = passwordHash.generate(Password);
        const userRecord = new UserStructure({
            UserName,
            Email,
            Password,
            UserType
        })
        userRecord.Password = pw;
        await userRecord.save();
        res.json({ message: "User Create" });

    } catch (error) {
        res.json({ message: "Cannot Create" });
    }
});

router.patch('/:userid', async (req, res) => {
    try {
        const updateds = await UserStructure.updateOne(
            { _id: req.params.userid },
            {
                $set: {
                    UserName: req.body.UserName,
                    Email: req.body.Email,
                    Passwords: req.body.Passwords,
                    UserType: req.body.UserType,
                }
            }
        );
        res.json(updateds);
    } catch (err) {
        res.json({ message: err });
    }
})

// delete user
router.delete('/:userid', async (req, res) => {
    try {
        res.json(await UserStructure.remove({ _id: req.params.userid }));
    } catch (err) {
        res.json({ message: "Cannot Delete User" });
    }
});


module.exports = router;