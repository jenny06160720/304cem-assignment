const express = require('express');
const router = express.Router();
const BranchModel = require('../models/BranchModel');

router.get("/", async (req, res) => {
    try {
        res.json(await BranchModel.find());
    } catch (err) {
        res.json({ message: err });
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await BranchModel.findById(req.params.id));
    } catch (err) {
        res.json({ message: err });
    }
});

router.post('/', async (req, res) => {
    const {enlocation, lat,lng} = req.body;
    try {
        const branchModel = new BranchModel({
            enlocation,
            lat,
            lng
        });
        const saves = await branchModel.save();
        res.json(saves);
    } catch(err) {
        res.json({ message: "Cannot Create" });
    }
});

router.delete('/:id', async (req, res) => {
    try {
        res.json(await BranchModel.remove({ _id: req.params.id }));
    } catch (err) {
        res.json({ message: "Cannot Delete branch" });
    }
});

router.put('/:id', async (req,res) => {
    try{
    const updateds = await BranchModel.updateOne(
        {_id: req.params.id}, 
        {$set: {
            enlocation: req.body.enlocation,
            lat: req.body.lat,
            lng: req.body.lng
        }}
     );
     res.json(updateds);    
    }catch (err) {
    res.json({ message: "Update Fail" });
    }
})

module.exports = router;