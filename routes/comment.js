const express = require('express');
const router = express.Router();
const CommentModel = require('../models/CommentModel');

router.get("/", async (req, res) => {
    try {
        res.json(await CommentModel.find());
    } catch (err) {
        res.json({ message: err });
    }
});

router.get("/getByUser/:Userid", async (req, res) => {
    try {
        res.json(await CommentModel.find({Userid:req.params.Userid}));
    } catch (err) {
        res.json({ message: err });
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await CommentModel.findById(req.params.id));
    } catch (err) {
        res.json({ message: err });
    }
});

router.post('/', async (req, res) => {
    const {Userid, Comment, CreateTime,UpdateTime} = req.body;
    try {
        const CommentModels = new CommentModel({
            Userid,
            Comment,
            CreateTime,
            UpdateTime
        });
        const saves = await CommentModels.save();
        res.json(saves);
    } catch(err) {
        console.log(err.message);
        res.status(500).send('Cannot Create New branch')
    }
});


router.patch('/:id', async (req, res) => {
    try {
        const updateds = await CommentModel.updateOne(
            { _id: req.params.id },
            {
                $set: {
                    Userid: req.body.Userid,
                    Comment: req.body.Comment,
                    CreateTime: req.body.CreateTime,
                    UpdateTime: new Date()
                }
            }
        );
        res.json(updateds);
    } catch (err) {
        res.json({ message: err });
    }
})


router.delete('/:id', async (req, res) => {
    try {
        res.json(await CommentModel.remove({ _id: req.params.id }));
    } catch (err) {
        res.json({ message: "Cannot Delete branch" });
    }
});


module.exports = router;