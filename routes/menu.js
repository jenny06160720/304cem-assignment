const express = require('express');
const router = express.Router();
const MenuModel = require('../models/MenuModel');

router.get("/", async (req, res) => {
    try {
        res.json(await MenuModel.find());
    } catch (err) {
        res.json({ message: "Fail" });
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await MenuModel.findById(req.params.id));
    } catch (err) {
        res.json({ message: err });
    }
});

router.post('/', async (req, res) => {
    const {name, price,urls,locations} = req.body;
    try {
        const menuModel = new MenuModel({
            name,
            price,
            urls,
            locations
        });
        const saves = await menuModel.save();
        res.json(saves);
    } catch(err) {
        res.json({ message: "Cannot Create" });
    }
});

router.delete('/:id', async (req, res) => {
    try {
        res.json(await MenuModel.remove({ _id: req.params.id }));
    } catch (err) {
        res.json({ message: "Cannot Delete menu" });
    }
});

router.put('/:id', async (req,res) => {
    try{
    const updateds = await MenuModel.updateOne(
        {_id: req.params.id}, 
        {$set: {
            name: req.body.name,
            price: req.body.price,
            urls: req.body.urls,
            locations:req.body.locations
        }}
     );
     res.json(updateds);    
    }catch (err) {
    res.json({message: err});
    }
})

module.exports = router;